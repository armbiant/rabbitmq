# rabbitmq

### Installation
To install this to your Minikube K8s Cluster run:
```shell
$ helm install rabbitmq . -n rabbitmq --create-namespace
```

### Design
We try to make our Helm Chart configurable, as much as possible, via the values.yaml
so you don't have to adjust the Helm Chart everytime you want to make a change.
Basic RabbitMQ Helm Chart
